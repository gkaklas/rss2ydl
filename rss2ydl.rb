#!/bin/env ruby

require 'rss'
require 'open-uri'
require 'youtube-dl'
require 'sequel'
require 'date'
require 'optparse'
require 'filesize'

options={}
OptionParser.new do |parser|
    parser.on("-f", "--feed FEED", "Feed to download videos from") do |feed|
        options[:feed]=feed
    end

    parser.on("-m", "--max-storage SIZE", "Maximum total size on disk to occupy with videos") do |maxstorage|
        options[:maxstorage]=maxstorage
    end

end.parse!

raise OptionParser::MissingArgument, "You must specify a feed (-f)" if options[:feed].nil?

DB = Sequel.connect('sqlite://videos.db')
DB.create_table? :videos do
    primary_key :id
    String :url
    String :filename
    Integer :filesize
    DateTime :downloaded
end

videos=DB[:videos]

open(options[:feed]) do |rss|
    feed=RSS::Parser.parse(rss)
    feed.items.each do |item|
        if not videos.where(:url => "#{item.link}").count.eql? 0
            next
        end
        video=YoutubeDL.download "#{item.link}"
        videos.insert(:url => "#{item.link}",
                      :filename => "#{video.filename}",
                      :filesize => File.size("#{video.filename}"),
                      :downloaded => DateTime.now)
        if not options[:maxstorage].empty?
            while videos.sum(:filesize) > Filesize.from(options[:maxstorage]).to_i
                oldest=videos.order(:downloaded).first
                File.delete(oldest[:filename])
                videos.where(:id => oldest[:id]).delete
            end
        end
    end
end
